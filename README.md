# OpenVDB Toolkit for Maya

This repository will contain pre-built binaries of the [OpenVDB](www.github.com/openvdb/openvdb) toolkit developed by Dreamworks Animation. I've added some small tweaks to the user experience which I will outline below. Something to note: __this is just the base toolkit__. For those of you who are familiar with Evgeny's work with the OpenVDB collection for [SOuP](www.soup-dev.com), this will most likely disappoint. There are only 9 nodes in this toolkit:

* Read  
* Write  
* Visualize  
* Copy  
* Filter  
* Transform  
* From Maya Fluid  
* From Polygons  
* To Polygons  

This means you will not be getting the same experience that you would with Evgeny's toolkit. If you can afford the subscription price, I highly recommend you buy it.

## Compatibility

Some notes upfront: I've compiled the library with ABI 3 compatibility using OpenVDB 4.0.2. If you would like to be bleeding edge, I'm sorry. I would prefer my files be render-able, so this is necessary. I know that Redshift, RenderMan, Mantra, and Arnold all use ABI 3, so the other engines probably do as well. Keep your eyes on the [VFX Platform](www.vfxplatform.com) though to see how things might change.

In terms of Maya, this build is only compatible with __Maya 2018__ on __Linux__. There is one big caveat at the moment with Viewport 2.0 not being supported. In order to use the plug-in you _must_ enable the legacy viewports by defining the environment variable `MAYA_ENABLE_LEGACY_VIEWPORT=1`. I am not a strong C++ developer yet, nor familiar with the Maya API so adding support is beyond me at the moment. If anyone qualified wants to help Dreamworks out, submit a pull request to the main repository!

I plan on adding support for Windows, but I'm pretty busy at the moment and Windows development is not my forte. Stay tuned for this if you're interested.

## Installing

In order to install this, either clone the repository or download the latest release from the [tags page](https://gitlab.com/omento/openvdb-toolkit/tags).

Place the extracted directory where ever you would like to store it, then copy the following to files to their respective locations:

`openvdb.mod` --> ~/maya/modules (or add it to your `MAYA_MODULE_PATH`)  
`shelf/shelf_OpenVDB.mel` --> ~/maya/2018/prefs/shelves/

Make sure to adjust the module file to point to the location of the packages root directory (the one containing `plug-ins`, `scripts`, etc).

Load the plug-in after launching Maya either by using the `loadPlugin "OpenVDB"` MEL command, loading it through the Plug-in Manager, or the easiest way: left-click on the OpenVDB symbol on the shelf.

## Using

I've done some work customizing the experience of the plug-in. Each node has a Node Editor template along with it, so you can get all of the common inputs/outputs you might need by pressing the 4 key. No more using the Connection Editor! Some nodes I intentionally left certain ports out of because they don't appear to be common items to be connecting external values to.

Along with this, instead of using the tab key in the Node Editor, I've set up a few useful commands when you use the shelf tool:

* When you left click on the OpenVDB symbol it will check to see if the plug-in is loaded and load it if it is not.  
* The Read, Write, and From Maya Fluid nodes will automatically hook up with the `time1` node.  
* Write, Copy, Filter, Transform, To Polygon, and Visualize will automatically hook up their VdbInput attribute based on the last currently selected OpenVDB node that has a VdbOutput attribute.  
* From Polygons will automatically hook up the meshInput attribute based on the last currently selected mesh object (or transform object containing a mesh item).  
* The Copy node will, depending on the selection, automatically hook up the last one or two currently selected OpenVDB nodes that have a VdbOutput attribute.  

If you want to take a look at these scripts, just right click on the shelf icon and choose 'Edit Popup'. They are also individually written out in the `scripts` directory.

Lastly, everything uses the OpenVDB logo in either SVG or PNG format. This will allow you to identify OpenVDB items in the shelf, Node Editor, and Outliner.
